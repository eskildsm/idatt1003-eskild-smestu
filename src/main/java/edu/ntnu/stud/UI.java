package edu.ntnu.stud;

import java.time.LocalTime;

public class UI {
    Entity[] entitiesTest;
    public void start() {
        System.out.println(entitiesTest[0]);
        System.out.println(entitiesTest[1]);
        System.out.println(entitiesTest[2]);
    }
    public void init() {
        entitiesTest = new Entity[3];

        entitiesTest[0] = new Entity(1);
        entitiesTest[1] = new Entity(2);
        entitiesTest[2] = new Entity(3);

        //entitiesTest[0].setDepartureTime(LocalTime.parse("00:00"));
        //entitiesTest[0].setLine("L1");
        //entitiesTest[0].setDestination("Oslo");
        //entitiesTest[0].setTrack(1);
        //entitiesTest[0].setDelay(LocalTime.parse("00:01"));

        entitiesTest[1].setDepartureTime(LocalTime.parse("00:00"));
        entitiesTest[1].setLine("L2");
        entitiesTest[1].setDestination("Bergen");
        entitiesTest[1].setTrack(2);
        entitiesTest[1].setDelay(LocalTime.parse("00:02"));

        entitiesTest[2].setDepartureTime(LocalTime.parse("00:00"));
        entitiesTest[2].setLine("L1");
        entitiesTest[2].setDestination("Trondheim");
        entitiesTest[2].setTrack(3);
        entitiesTest[2].setDelay(LocalTime.parse("00:03"));
    }
}

package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.Objects;

/**
 * This class is used to store data regarding a train departure
 * It is used by another class to keep track over: train departures
 * trainNumber variable is final because this instances of this class is linked to one train, and trainNumber should therefore not change
 * If values are not set, they become null, and is translated to the user as "unknown" in toString

 * Values:
 * departureTime (LocalTime) is the time the train is scheduled to depart, and is of datatype LocalTime because it is a time
 * line (String) is the line the train is on, and is of datatype String because line is given as text
 * trainNumber (int) is the number of the train, and is of datatype int because it is a number, and is set to -1 as default because it is not known
   If value is -1 trainNumber will be shown as "unknown" in toString
 * destination (String) is the final destination of the train, and is of datatype String because destination is given as text
 * track (int) is the track the train is on, and is of datatype int because it is a number, and is set to -1 as default because it is not known
   If value is -1 trainNumber will be shown as "unknown" in toString
 * delay (LocalTime) is the delay of the train, and is of datatype LocalTime because it is a time, and is set to 00:00 as default because it is unknown
 */
public class Entity {
    private LocalTime departureTime;
    private String line;
    private final int trainNumber;
    private String destination;
    private int track = -1;
    private LocalTime delay = LocalTime.parse("00:00");

    public Entity(int trainNumber) {
        this.trainNumber = trainNumber;

    }


    /**
     * This method is used to set the departure time of the train
     * @param departureTime the time the train is scheduled to depart
     */
    public void setDepartureTime(LocalTime departureTime) {
        this.departureTime = departureTime;
    }

    /**
     * This method is used to set the line the train is on
     * @param line the line the train is on
     * @throws IllegalArgumentException if line is empty
     */
    public void setLine(String line) throws IllegalArgumentException{
        if(Objects.equals(line, ""))
            throw new IllegalArgumentException("Line must have a non empty name");
        this.line = line;
    }

    /**
     * This method is used to set the final destination of the train
     * @param destination the final destination of the train
     */
    public void setDestination(String destination) {
        if(Objects.equals(line, ""))
            throw new IllegalArgumentException("Destination must have a non empty name");
        this.destination = destination;
    }

    /**
     * This method is used to set the track the train is on
     * @param track the track the train is on, default is -1
     */
    public void setTrack(int track) {
        this.track = track;
    }

    /**
     * This method is used to set the delay of the train
     * @param delay the delay of the train, default is 00:00
     */
    public void setDelay(LocalTime delay) {
        this.delay = delay;
    }

    /**
     * This method is used to get the departure time of the train
     * @return departureTime: the time the train is scheduled to depart
     */
    public LocalTime getDepartureTime() {
        return departureTime;
    }

    /**
     * This method is used to get the line the train is on
     * @return line: the line the train is on
     */
    public String getLine() {
        return line;
    }

    /**
     * This method is used to get the number of the train
     * @return trainNumber: the number of the train
     */
    public int getTrainNumber() {
        return trainNumber;
    }

    /**
     * This method is used to get the final destination of the train
     * @return destination: the final destination of the train
     */
    public String getDestination() {
        return destination;
    }

    /**
     * This method is used to get the track the train is on
     * @return track: the track the train is on
     */
    public int getTrack() {
        return track;
    }

    /**
     * This method is used to get the delay of the train
     * @return delay: the delay of the train
     */
    public LocalTime getDelay() {
        return delay;
    }

    /**
     * This method is used to print the values of the train departure
     * @return a string containing the values of the train departure and values will be "unknown" if not set
     */
    @Override
    public String toString() {
        return "Departure Time=" + (this.departureTime == null ? "unknown" : this.departureTime.toString()) +
                ", Line='" + (this.line == null ? "unknown" : this.line) + '\'' +
                ", Train Number=" + (this.trainNumber == -1 ? "unknown" : this.trainNumber) +
                ", Destination='" + (this.destination == null ? "unknown" : this.destination) + '\'' +
                ", Track=" + (this.track == -1 ? "unknown" : this.track) +
                ", Delay=" + this.delay.toString();
    }
}
